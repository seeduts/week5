﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EngineerDefault.aspx.cs" Inherits="Assignment1.EngineerDefault" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .Logout{
            color: red; 
            text-decoration: underline;
        }
        .Logout:hover{
            cursor:pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <form runat="server">
    <asp:label ID="labelWelcome"  runat="server" text=""></asp:label>
        <br />
   <asp:LinkButton ID="LinkButton1" runat="server" 
    CssClass="Logout" OnClick="LinkButton1_Click">Logout</asp:LinkButton>
        <br /> <br /> 
    <asp:menu runat="server">
        <Items>
            <asp:MenuItem NavigateUrl="~/ChangePassword.aspx" Text="ChangePassword" Value="ChangePassword"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/CreateClient.aspx" Text="Create Client" Value="Create Client"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/ViewClients.aspx" Text="List Clients" Value="ListClients"></asp:MenuItem>
            <asp:MenuItem Text="List Intervention" Value="ListIntervention"></asp:MenuItem>
        </Items>
    </asp:menu>
        </form>
</asp:Content>
