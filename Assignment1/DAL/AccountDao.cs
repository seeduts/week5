﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1.DAL
{
    /// <summary>
    /// This is a derived class inherited from UserDao used to access and update accountant data from database
    /// </summary>
    public class AccountDao : UserDao
    {

        public DataSet getStaff()
        {
            DataSet ds = new DataSet();
            string query = "Select Users.Name as staffName,UserType.Name as Role from Users inner join UserType on Users.UserTypeID=UserType.ID where UserTypeID<>3";
            return conn.executeSelectDataSetQuery(query);
        }

        public bool UpdateStaffDistrict(string name, int districtId)
        {
            string query = "Update Users set DistrictID=@districtId where Name=@name";
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@districtId", SqlDbType.Int);
            sqlParameters[0].Value = districtId;
            sqlParameters[1] = new SqlParameter("@name", SqlDbType.VarChar);
            sqlParameters[1].Value = name;
            return conn.executeUpdateQuery(query, sqlParameters);
        }

        public DataSet getTotalCosts()
        {
            string query = "Select sum(Intervention.LabourRequired) as totalHours,sum(Intervention.Costs) as totalCosts,users.name from Intervention inner join Users on users.ID = Intervention.ProposerID where users.UserTypeID = 1 and Intervention.InterventionState = 'Completed' group by Intervention.ProposerID ,users.Name order by users.Name";
            return conn.executeSelectDataSetQuery(query);
        }

        public DataSet getAverageCosts()
        {
            string query = "Select avg(Intervention.LabourRequired) as AverageHours,avg(Intervention.Costs) as AverageCosts,users.name from Intervention inner join Users on users.ID = Intervention.ProposerID where users.UserTypeID = 1 and Intervention.InterventionState = 'Completed' group by Intervention.ProposerID ,users.Name order by users.Name";
            return conn.executeSelectDataSetQuery(query);
        }

        public DataSet getDistrictsCosts()
        {
            string query = "select d.Name, sum(i.LabourRequired) as TotalLabour,sum(i.Costs) As TotalCosts from Districts d, Intervention i, Clients c where d.ID = c.DistrictID and c.ID = i.ClientID and i.InterventionState = 'Completed'group by d.Name";
            return conn.executeSelectDataSetQuery(query);
        }

        public DataSet getMonthlyCosts()
        {
            string query = "SELECT DATEPART(mm,i.PerformDate)as Month,sum(i.LabourRequired)as TotalLabour,sum(i.Costs) As TotalCosts FROM intervention i where i.InterventionState = 'Completed' GROUP BY DATEPART(mm, i.PerformDate)";
            return conn.executeSelectDataSetQuery(query);
        }
    }
}