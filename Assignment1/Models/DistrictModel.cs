﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1.Models
{
    /// <summary>
    ///  A model object for district, storing their id and name.
    /// </summary>
    public class DistrictModel
    {
        private int id;
        private string name;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}