﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class Default1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] == null)
                Response.Redirect("Login.aspx");
            labelWelcome.Text = "Welcome " + Session["UserName"].ToString();
            int userTypeID =(int) Session["UserTypeId"];
            if (userTypeID == 1)
                ((Menu)Master.FindControl("menuEngineer")).Visible = true;
            else if (userTypeID == 2)
                ((Menu)Master.FindControl("menuManager")).Visible = true;
            else ((Menu)Master.FindControl("menuAccount")).Visible = true;
        }

        protected void LinkButtonLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Login.aspx");
        }
    }
}