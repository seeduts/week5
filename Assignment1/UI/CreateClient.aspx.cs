﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;

namespace Assignment1
{
    public partial class CreateClient : System.Web.UI.Page
    {
        EngineerBL engieerBL = new EngineerBL();
        protected void Page_Load(object sender, EventArgs e)
        {
           ((Menu)Master.FindControl("menuEngineer")).Visible = true;
            int districtId = (int)Session["DistrictId"];
            ddlDistrict.DataSource = engieerBL.viewDistrictsName(districtId);
            ddlDistrict.DataTextField = "Name";
            ddlDistrict.DataValueField = "ID";
            ddlDistrict.DataBind();
        }

        protected void buttonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UI/Default.aspx");
        }

        protected void buttonSubmit_Click(object sender, EventArgs e)
        {
            int userID = (int)Session["userID"];
            if (engieerBL.createClient(txtClientname.Text, txtLocation.Text,int.Parse(ddlDistrict.SelectedValue), userID)==false)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('This Client already existed!')</script>");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect",
                     "alert('Create new Client successfully'); window.location='" +
                     Request.ApplicationPath + "UI/Default.aspx';", true);
            }
        }
    }
}