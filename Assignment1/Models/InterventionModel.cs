﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1.Models
{
    /// <summary>
    ///  A model object for intervention, storing their id, type id,
    /// type,client id, labour required, costs, proposer id, proposer and perform date, intervention state,
    /// approver id and name, notes, life reamining and recent visit date.
    /// </summary>
    public class InterventionModel
    {
        private int iD;
        private int interventionTypeID;
        private string interventionType;
        private int clientID;
        private float labourRequired;
        private float costs;
        private int proposerID;
        private string proposer;
        private DateTime performDate;
        private string interventionState;
        private int approverID;
        private string approver;
        private string notes;
        private int lifeRemaining;
        private DateTime recentVisitDate;

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }

        public int InterventionTypeID
        {
            get
            {
                return interventionTypeID;
            }

            set
            {
                interventionTypeID = value;
            }
        }

        public int ClientID
        {
            get
            {
                return clientID;
            }

            set
            {
                clientID = value;
            }
        }

        public float LabourRequired
        {
            get
            {
                return labourRequired;
            }

            set
            {
                labourRequired = value;
            }
        }

        public float Costs
        {
            get
            {
                return costs;
            }

            set
            {
                costs = value;
            }
        }

        public int ProposerID
        {
            get
            {
                return proposerID;
            }

            set
            {
                proposerID = value;
            }
        }

        public DateTime PerformDate
        {
            get
            {
                return performDate;
            }

            set
            {
                performDate = value;
            }
        }

        public string InterventionState
        {
            get
            {
                return interventionState;
            }

            set
            {
                interventionState = value;
            }
        }

        public int ApproverID
        {
            get
            {
                return approverID;
            }

            set
            {
                approverID = value;
            }
        }

        public string Notes
        {
            get
            {
                return notes;
            }

            set
            {
                notes = value;
            }
        }

        public int LifeRemaining
        {
            get
            {
                return lifeRemaining;
            }

            set
            {
                lifeRemaining = value;
            }
        }

        public DateTime RecentVisitDate
        {
            get
            {
                return recentVisitDate;
            }

            set
            {
                recentVisitDate = value;
            }
        }

        public string InterventionType
        {
            get
            {
                return interventionType;
            }

            set
            {
                interventionType = value;
            }
        }

        public string Proposer
        {
            get
            {
                return proposer;
            }

            set
            {
                proposer = value;
            }
        }

        public string Approver
        {
            get
            {
                return approver;
            }

            set
            {
                approver = value;
            }
        }
    }
}