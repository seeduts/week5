﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Assignment1.DAL
{
    /// <summary>
    /// This is used to interact with database to access user data
    /// </summary>
    public class UserDao
    {
        protected dbConnection conn;

        //constructor
        public UserDao()
        {
            conn = new dbConnection();
        }

        /// <summary>
        /// Used to retrieve user data by the name when a user logins in
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public DataTable SearchByName(string username)
        {
            string query = "Select * from Users where UserName=@userName COLLATE SQL_Latin1_General_CP1_CS_AS";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@userName", SqlDbType.NChar);
            sqlParameters[0].Value = username;
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <summary>
        /// Used to retrieve data by the name of staff 
        /// </summary>
        /// <param name="staffname"></param>
        /// <returns></returns>
        public DataTable SearchByStaffName(string staffname)
        {
            string query = "Select * from Users where Name=@staffName COLLATE SQL_Latin1_General_CP1_CS_AS";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@staffName", SqlDbType.NChar);
            sqlParameters[0].Value = staffname;
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <summary>
        /// Used to update Password of a user
        /// </summary>
        /// <param name="password"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public bool ChangePassword(string password, int userID)
        {
            string query = "update Users set password=@password where ID=@userID";
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@password", SqlDbType.NChar);
            sqlParameters[0].Value = password;
            sqlParameters[1] = new SqlParameter("@userID", SqlDbType.Int);
            sqlParameters[1].Value = userID;
            return conn.executeUpdateQuery(query, sqlParameters);
        }

        /// <summary>
        /// Used to retrieve staff full name
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string getStaffName(int userID)
        {
            string query = "Select Name from Users where ID=@userID";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@userID", SqlDbType.Int);
            sqlParameters[0].Value = userID;
            string name = conn.executeSelectQuery(query, sqlParameters).Rows[0][0].ToString();
            return name;
        }
    }
}