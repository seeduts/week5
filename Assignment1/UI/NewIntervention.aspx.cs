﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;
using System.Data;
using Assignment1.Models;

namespace Assignment1
{
    public partial class CreateIntervention : System.Web.UI.Page
    {
        EngineerBL engieerBL = new EngineerBL();
        string name;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                name = (string)Session["ClientName"];
                txtClient.Text = name;
                ddlInterventionType.DataSource = engieerBL.ViewInterventionType();
                ddlInterventionType.DataTextField = "Name";
                ddlInterventionType.DataValueField = "ID";
                ddlInterventionType.DataBind();
                txtLabourRequired.Text = engieerBL.ViewInterventionType().Rows[0][2].ToString();
                txtCostRequired.Text = engieerBL.ViewInterventionType().Rows[0][3].ToString();
                txtProposer.Text = Session["StaffName"].ToString();
                CalendarPerformDate.SelectedDate = DateTime.Today;
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void ddlInterventionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id =Convert.ToInt32(ddlInterventionType.SelectedValue);
            var row = engieerBL.ViewInterventionType().AsEnumerable().Where(x => x.Field<int>("ID") == id);
            txtLabourRequired.Text = row.First()["EstimatedHours"].ToString();
            txtCostRequired.Text = row.First()["EstimatedCosts"].ToString();
        }

        protected void PerformDateValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if( CalendarPerformDate.SelectedDate <DateTime.Today) 
            {
                args.IsValid = false;
                PerformDateValidator.ErrorMessage = "Perform date cannot smaller than today.";
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
           if (PerformDateValidator.IsValid == true)
            {
                InterventionModel interventionModel=new InterventionModel();
                interventionModel.ClientID =Convert.ToInt32(Session["ClientID"]);
                interventionModel.InterventionTypeID = Convert.ToInt32(ddlInterventionType.SelectedValue);
                interventionModel.LabourRequired= Convert.ToInt32(txtLabourRequired.Text);
                interventionModel.Costs = Convert.ToInt32(txtCostRequired.Text);
                interventionModel.ProposerID = Convert.ToInt32(Session["userID"]);
                interventionModel.InterventionState = ddlState.SelectedValue;
                interventionModel.PerformDate= CalendarPerformDate.SelectedDate;
                interventionModel.Notes = txtComments.Text;
                if (engieerBL.CreateIntervenion(interventionModel)==false)
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "<script language='javascript'>alert('Create new Intervetion failed!')</script>");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect",
                     "alert('Create new Intervention successfully'); window.location='" +
                     Request.ApplicationPath + "UI/Default.aspx';", true);
                }
            }
        }

        protected void ButtonCheck_Click(object sender, EventArgs e)
        {

            int hours =int.Parse(txtLabourRequired.Text);
            int costs = int.Parse(txtCostRequired.Text);
            int interventionTypeID =int.Parse(ddlInterventionType.Text);
            if (engieerBL.checkCreateAuthority(hours, costs, txtProposer.Text, interventionTypeID) == true)
            {
                ddlState.Enabled = true;
                LabelCheck.Text = "You have the authority to approve and complete the Intervention.";
            }
            else
            {
                LabelCheck.Text = "You should wait for the manager to approve the Intervention.";
                ddlState.Enabled = false;
            }
        }
    }
}