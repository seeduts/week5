﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;

namespace Assignment1.UI
{
    public partial class RunReport : System.Web.UI.Page
    {
        AccountBL accountBL = new AccountBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Menu)Master.FindControl("menuAccount")).Visible = true;
        }

        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            if(Menu1.SelectedItem.Value=="1")
            {
                GridView1.DataSource = accountBL.ViewTotalCostsByEnigneer();
                GridView1.DataBind();
            }
            else if(Menu1.SelectedItem.Value=="2")
            {
                GridView1.DataSource = accountBL.ViewAverageCostsByEngineer();
                GridView1.DataBind();
            }
            else if(Menu1.SelectedItem.Value=="3")
            {
                GridView1.DataSource = accountBL.ViewDistrictCosts();
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = accountBL.ViewMonthlyCosts();
                GridView1.DataBind();
            }
        }
    }
}