﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1.DAL
{
    /// <summary>
    /// This is used to access and update district data from database
    /// </summary>
    public class DistrictDao
    {
        private dbConnection conn;
        public DistrictDao()
        {
            conn = new dbConnection();
        }
        /// <summary>
        /// get the name of a district given the district id
        /// </summary>
        public DataTable getDistrictName(int districtId)
        {
            string query = "Select * from Districts where ID=@id";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@id", SqlDbType.Int);
            sqlParameters[0].Value = districtId;
            return conn.executeSelectQuery(query, sqlParameters);
        }
        /// <summary>
        /// get the districts
        /// </summary>
        /// <returns></returns>
        public DataTable getDistricts()
        {
            string query= "Select* from Districts";
            return conn.executeSelectQuery(query);
        }

        /// <summary>
        /// get the distict id a staff belongs to given the name of the district
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int getStaffDistrictID(string name)
        {
            string query = "Select * from Users where Name=@name";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@name", SqlDbType.NChar);
            sqlParameters[0].Value = name;
            DataTable dt = conn.executeSelectQuery(query,sqlParameters);
            int districtId = int.Parse(dt.Rows[0]["DistrictID"].ToString());
            return districtId;
        }
    }
}