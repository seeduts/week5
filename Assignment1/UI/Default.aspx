﻿<%@ Page Title="" Language="C#" MasterPageFile="../UI/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment1.Default1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
     <style>
        .Logout{
            color: red; 
            text-decoration: underline;
        }
        .Logout:hover{
            cursor:pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
     <asp:label ID="labelWelcome"  runat="server" text=""></asp:label>
        <br />
   <asp:LinkButton ID="LinkButtonLogout" runat="server" 
    CssClass="Logout" OnClick="LinkButtonLogout_Click">Logout</asp:LinkButton>
        <br />
    
</asp:Content>
