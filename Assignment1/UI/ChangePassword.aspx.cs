﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;
namespace Assignment1
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] == null)
                Response.Redirect("Login.aspx");
            int userTypeID = (int)Session["UserTypeId"];
            if (userTypeID == 1)
                ((Menu)Master.FindControl("menuEngineer")).Visible = true;
            else if (userTypeID == 2)
                ((Menu)Master.FindControl("menuManager")).Visible = true;
            else ((Menu)Master.FindControl("menuAccount")).Visible = true;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            UserBL userBL = new UserBL();
            int userID = (int)Session["userID"];
            if (txtConfirmPassword.Text != txtnewPassword.Text)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Passwords do not match!')</script>");
            }
            else
            {
                if (userBL.ChangePassword(userID,txtConfirmPassword.Text)==false)
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Submit failed')</script>");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect",
                    "alert('Change password success'); window.location='" +
                    Request.ApplicationPath + "Default.aspx';", true);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}