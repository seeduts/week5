﻿<%@ Page Title="" Language="C#" MasterPageFile="../UI/MasterPage.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Assignment1.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="css/ChangePassword.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
     <section class="ChangePassword">
<div class="form_title">Change Password</div>
     <asp:Label ID="LabelnewPassword" ForeColor="White" CssClass="Label" runat="server" Text="new password"></asp:Label>
     <asp:TextBox ID="txtnewPassword" runat="server" CssClass="Change_password" TextMode="Password" MaxLength="50"></asp:TextBox>
     <br />
     <asp:Label ID="LabelConfirmPassword" ForeColor="White" CssClass="Label" runat="server" Text="Confirm new Password" ></asp:Label>
     <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="Change_password" TextMode="Password" MaxLength="50"></asp:TextBox>
     <br />
     <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
      <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
</section>
</asp:Content>
