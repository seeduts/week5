﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment1.DAL;
using Assignment1.Models;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace Assignment1.BLL
{
    public class UserBL
    {
        private UserDao userDao;
        protected DistrictDao districtDao;
        public UserBL()
        {
            userDao = new UserDao();
            districtDao = new DistrictDao();
        }

        //gets user by username
        public virtual UserModel getUserByUserName(string userName)
        {
            UserModel userModel = new UserModel();
            DataTable dt = new DataTable();
            dt = userDao.SearchByName(userName);
            foreach(DataRow dr in dt.Rows)
            {
                userModel.ID = int.Parse(dr[0].ToString());
                userModel.Name = dr[1].ToString();
                userModel.UserName = dr[2].ToString();
                userModel.Password = dr[3].ToString();
                userModel.UserTypeID = int.Parse(dr[4].ToString());
                if(userModel.UserTypeID!=3)
                {
                    userModel.DistrictID = int.Parse(dr[5].ToString());
                    userModel.MaximumHours = int.Parse(dr[6].ToString());
                    userModel.MaximumCosts = int.Parse(dr[7].ToString());
                }
            }
            return userModel;
        }


        //returns user details by staff name
        public UserModel getUserByStaffName(string staffName)
        {
            UserModel userModel = new UserModel();
            DataTable dt = new DataTable();
            dt = userDao.SearchByStaffName(staffName);
            foreach (DataRow dr in dt.Rows)
            {
                userModel.ID = int.Parse(dr[0].ToString());
                userModel.Name = dr[1].ToString();
                userModel.UserName = dr[2].ToString();
                userModel.Password = dr[3].ToString();
                userModel.UserTypeID = int.Parse(dr[4].ToString());
               userModel.DistrictID = int.Parse(dr[5].ToString());
               userModel.MaximumHours = int.Parse(dr[6].ToString());
               userModel.MaximumCosts = int.Parse(dr[7].ToString());
            }
            return userModel;
        }

        //return true if login successful and false if unsuccess login attempt
        public bool Login(string username,string password)
        {
            
            UserModel userModel = getUserByUserName(username);
            if (userModel.Name==null) return false;
            else
            {
                string passWord = "0x" + GetMd5Hash(MD5.Create(), password);
                if (userModel.Password == passWord) return true;
                else return false;
            }
        }

        //returns true if password change is successfull and false if it fails
        public bool ChangePassword(int userID,string password)
        {
            string Password = "0x" + GetMd5Hash(MD5.Create(), password);
            return userDao.ChangePassword(Password, userID);
        }

        // retuns datatable of district table from database
        public DataTable getDistricts()
        {
            return districtDao.getDistricts();
        }

        // retuns staff name from staff id
        public string getStaffName(int userID)
        {
           return userDao.getStaffName(userID);
        }
        
        //change the input into md5 encryption 
        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2").ToUpper());
            }
            return sBuilder.ToString();
        }
    }
}