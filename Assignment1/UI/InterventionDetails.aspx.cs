﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.Models;
using Assignment1.BLL;

namespace Assignment1.UI
{
    public partial class InterventionDetails : System.Web.UI.Page
    {
        EngineerBL engineerBL = new EngineerBL();
        InterventionModel interventionmodel;
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Menu)Master.FindControl("menuEngineer")).Visible = true;
            if (Session["UserName"] == null)
                Response.Redirect("Login.aspx");
            if(!IsPostBack)
            {
                int clientID =(int)Session["ClientID"];
                int InterventionTypeID = int.Parse(Session["InterventionTypeID"].ToString());
              interventionmodel = engineerBL.ViewInterventionDetails(clientID, InterventionTypeID);
                txtClient.Text = Session["ClientName"].ToString();
                txtInterventionType.Text = Session["InterventionType"].ToString();
                txtLabourRequired.Text=interventionmodel.LabourRequired.ToString();
                txtCostRequired.Text = interventionmodel.Costs.ToString();
                txtProposer.Text = interventionmodel.Proposer;
                CalendarPerformDate.SelectedDate = interventionmodel.PerformDate;
                CalendarPerformDate.VisibleDate = CalendarPerformDate.SelectedDate;
                ddlState.SelectedValue = interventionmodel.InterventionState.Trim();
                txtApprover.Text = interventionmodel.Approver;
                txtComments.Text = interventionmodel.Notes;
                txtLifeRemaining.Text = interventionmodel.LifeRemaining.ToString();
                Calendar2.VisibleDate = interventionmodel.RecentVisitDate;
                Calendar2.SelectedDate = interventionmodel.RecentVisitDate;     Session["interventionID"] = interventionmodel.ID;  
            }
            if (txtProposer.Text == Session["StaffName"].ToString())
                ddlState.Enabled = true;
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (recentVistValidator.IsValid == true)
            {
                if (engineerBL.QualityManagement(txtComments.Text, int.Parse(txtLifeRemaining.Text), Calendar2.SelectedDate, int.Parse(Session["interventionID"].ToString())) == false)
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Submit failed')</script>");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "redirect",
                       "alert('Update Intervention Successfully'); window.location='" +
                       Request.ApplicationPath + "UI/Default.aspx';", true);
                }
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void recentVistValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Calendar2.SelectedDate <CalendarPerformDate.SelectedDate || Calendar2.SelectedDate > DateTime.Today)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }
}