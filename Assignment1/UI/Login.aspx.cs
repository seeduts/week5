﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;
using Assignment1.Models;

namespace Assignment1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonReset_Click(object sender, EventArgs e)
        {
            TxtUsername.Text = "Username";
            TxtPassword.Text = "Password";
        }
        protected void buttonLogin_Click(object sender,EventArgs e)
        {
            UserBL userBL = new UserBL();
            
            if (userBL.Login(TxtUsername.Text,TxtPassword.Text)==false)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Invalid Username and Password')</script>");
                TxtUsername.Text = "Username";
                TxtPassword.Text = "Password";
            }
            else
            {
                UserModel userModel = userBL.getUserByUserName(TxtUsername.Text);
                Session["UserName"] = userModel.UserName;
                Session["userID"] = userModel.ID;
                Session["UserTypeId"] = userModel.UserTypeID;
                Session["DistrictId"] = userModel.DistrictID;
                Session["StaffName"] = userModel.Name;
                Response.Redirect("Default.aspx");
            }
        }


    }
}