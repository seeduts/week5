﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1.Models
{
    /// <summary>
    ///  A model object for user, storing storing their id, name,
    /// username ,password, user type, costs, district , proposer and perform date, intervention state,
    /// approver id and name, notes, life reamining and recent visit.
    /// </summary>
    public class UserModel
    {
        private int id;
        private string name;
        private string userName;
        private string password;
        private int userTypeID;
        private string userType;
        private int districtID;
        private int maximumHours;
        private int maximumCosts;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public int UserTypeID
        {
            get { return userTypeID; }
            set { userTypeID = value; }
        }

        public string UserType
        {
            get { return userType; }
            set { userType = value; }
        }
        

        public int DistrictID
        {
            get { return districtID;}
            set { districtID = value; }
        }

        public int MaximumHours
        {
            get { return maximumHours;}
            set { maximumHours = value; }
        }

        public int MaximumCosts
        {
            get { return maximumCosts; }
            set { maximumCosts = value; }
        }
       
    }
}