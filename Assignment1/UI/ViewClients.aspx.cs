﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;
using Assignment1.Models;

namespace Assignment1
{
    public partial class ViewClients : System.Web.UI.Page
    {
        EngineerBL engineerBL = new EngineerBL();

        protected void Page_Load(object sender, EventArgs e)
        {
            ((Menu)Master.FindControl("menuEngineer")).Visible = true;
            if (Session["UserName"] == null)
                Response.Redirect("Login.aspx");
            int districtID = (int)Session["DistrictId"];
            if (!IsPostBack)
            {             
                    ddlDistrict.DataSource = engineerBL.getDistricts(); ;
                    ddlDistrict.DataTextField = "Name";
                    ddlDistrict.DataValueField = "ID";
                    ddlDistrict.DataBind();
                if (engineerBL.viewClients(districtID).Tables[0].Rows.Count > 0)
                {
                    ClientsList.DataSource = engineerBL.viewClients(districtID);
                    ClientsList.DataBind();
                }
                else
                {
                    Message.Text = "No Clients in your district";
                }
            }
        }

        protected void ClientsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PanelClientDetail.Visible = true;
            GridViewRow row = ClientsList.SelectedRow;
            int ID =int.Parse(row.Cells[1].Text.Trim());
            ClientModel clientModel = engineerBL.viewClientDetail(ID);
            txtName.Text = clientModel.Name;
            txtLocation.Text = clientModel.LocationInfo;
            ddlDistrict.SelectedValue = clientModel.DistrictID.ToString();
            int userID = clientModel.UserID;
            Session["ClientID"] = clientModel.Id;
            txtCreateStaff.Text = engineerBL.getStaffName(userID);
            if (engineerBL.viewIntervention(clientModel.Id).Rows.Count > 0)
            {
                ddlIntervention.Visible = true;
                LabelAlert.Visible = false;
                ddlIntervention.DataSource = engineerBL.viewIntervention(clientModel.Id);
                ddlIntervention.DataTextField = "TypesName";
                ddlIntervention.DataValueField = "TypesID";
                ddlIntervention.DataBind();
            }
            else
            {
                LabelAlert.Visible = true;
                ddlIntervention.Visible = false;
                LabelAlert.Text = "This Client does not have any Interventions!";
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void ButtonNewIntervention_Click(object sender, EventArgs e)
        {
            Session["ClientName"] = txtName.Text;          
            Response.Redirect("NewIntervention.aspx");
        }

        protected void ButtonInterventionDetail_Click(object sender, EventArgs e)
        {
            if (ddlIntervention.Visible==false)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "<script language='javascript'>alert('This Client does not has any intervention!')</script>");
            }
            else
            {
                Session["ClientName"] = txtName.Text;
                Session["InterventionTypeID"] = ddlIntervention.SelectedValue;
                Session["InterventionType"] = ddlIntervention.SelectedItem.Text;
                Response.Redirect("InterventionDetails.aspx");
            }
        }

    
    }
}