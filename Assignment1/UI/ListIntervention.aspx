﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.Master" AutoEventWireup="true" CodeBehind="ListIntervention.aspx.cs" Inherits="Assignment1.ListIntervention" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <asp:label id="Message" forecolor="Red" runat="server"/>
    <asp:GridView ID="ViewIntervention" runat="server" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" OnSelectedIndexChanged="ViewIntervention_SelectedIndexChanged">
    <Columns>
        <asp:CommandField HeaderText="Change State" SelectText="Edit" ShowSelectButton="True" />
    </Columns>
    <FooterStyle BackColor="White" ForeColor="#000066" />
    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
    <RowStyle ForeColor="#000066" />
    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
    <SortedAscendingCellStyle BackColor="#F1F1F1" />
    <SortedAscendingHeaderStyle BackColor="#007DBB" />
    <SortedDescendingCellStyle BackColor="#CAC9C9" />
    <SortedDescendingHeaderStyle BackColor="#00547E" />
</asp:GridView><br />
    <asp:panel ID="panelModify" runat="server" Visible="false">
    <asp:Label ID="LabelClient" runat="server" Text="Client" CssClass="label"></asp:Label>
    <asp:TextBox ID="txtClient" runat="server" Enabled="False" CssClass="textBox"></asp:TextBox>
        
    <asp:Label ID="LabelState" runat="server" Text="State" Height="22px"></asp:Label>
    <asp:DropDownList ID="ddlState" runat="server" Height="22px">
        <asp:ListItem>Proposed</asp:ListItem>
        <asp:ListItem>Approved</asp:ListItem>
        <asp:ListItem>Cancelled</asp:ListItem>
        <asp:ListItem>Completed</asp:ListItem>
    </asp:DropDownList>
         <br />
    <asp:Label ID="LabelInterventionType" runat="server" Text="InterventionType"></asp:Label>
         <asp:TextBox ID="txtInterventionType" runat="server" Enabled="False" Width="183px" CssClass="textBox"></asp:TextBox>
    <asp:Label ID="LabelApprover" runat="server" Text="Approver" Height="22px"></asp:Label>
    <asp:TextBox ID="txtApprover" runat="server" Enabled="False"></asp:TextBox>
         <br />
    <asp:Label ID="LabelLabourRequired" runat="server" Text="Labour Required (Hours)"></asp:Label>
    <asp:TextBox ID="txtLabourRequired" runat="server" Enabled="False" CssClass="textBox"></asp:TextBox>
     <asp:Label ID="LabelCostRequired" runat="server" Text="Cost Required (AUD$)"></asp:Label>
    <asp:TextBox ID="txtCostRequired" runat="server" Enabled="False"></asp:TextBox>
    <br />
     <asp:Label ID="LabelProposer" runat="server" Text="Proposer"></asp:Label>
    <asp:TextBox ID="txtProposer" runat="server" Enabled="false" CssClass="textBox"></asp:TextBox>
    <asp:Label ID="LabelLifeRemaining" runat="server" Text="LifeRemaining"></asp:Label>
    <asp:TextBox ID="txtLifeRemaining" runat="server" MaxLength="4" Enabled="False"></asp:TextBox>
         <br />
    <asp:Label ID="LabelComments" runat="server" Text="Comments" style="margin-top:0px"></asp:Label><br />
    <asp:TextBox id="txtComments" TextMode="multiline" Columns="50" Rows="5" runat="server" MaxLength="10000" Enabled="False"  />
         <br />
    <asp:Label ID="LabelPerformDate" runat="server" Text="Perform Date" ></asp:Label>
      <asp:Label ID="LabelRecentVist" runat="server" Text="RecentVist" style="margin-left:210px"></asp:Label>
    <asp:Calendar ID="CalendarPerformDate" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" ShowGridLines="True" Width="220px" Enabled="False">
        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
        <OtherMonthDayStyle ForeColor="#CC9966" />
        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
        <SelectorStyle BackColor="#FFCC66" />
        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
  
    <asp:Calendar ID="Calendar2" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" ShowGridLines="True" Width="220px" style="margin-top:-200px;margin-left:300px" Enabled="False">
        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
        <OtherMonthDayStyle ForeColor="#CC9966" />
        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
        <SelectorStyle BackColor="#FFCC66" />
        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
    <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"  />
    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
        </asp:panel>
</asp:Content>
