﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage.Master" AutoEventWireup="true" CodeBehind="ViewClients.aspx.cs" Inherits="Assignment1.ViewClients" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
      <asp:label id="Message" forecolor="Red" runat="server"/>
      <br/>   
    <asp:GridView ID="ClientsList" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" OnSelectedIndexChanged="ClientsList_SelectedIndexChanged">
        <Columns>
            <asp:CommandField HeaderText="View Details" ShowHeader="True" ShowSelectButton="True" SelectText="Detail" />
        </Columns>
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FFF1D4" />
        <SortedAscendingHeaderStyle BackColor="#B95C30" />
        <SortedDescendingCellStyle BackColor="#F1E5CE" />
        <SortedDescendingHeaderStyle BackColor="#93451F" />
</asp:GridView><br />
    <asp:Panel ID="PanelClientDetail" runat="server" Visible="false">
    <asp:Label ID="LabelName" runat="server" Text="Name"></asp:Label>
    <asp:TextBox ID="txtName" runat="server" Enabled="False"></asp:TextBox><br />
    <asp:Label ID="LabelLocation" runat="server" Text="LocationInfo"></asp:Label>
    <asp:TextBox ID="txtLocation" runat="server" Enabled="False"></asp:TextBox><br />
    <asp:Label ID="LabelDistrict" runat="server" Text="District"></asp:Label>
    <asp:DropDownList ID="ddlDistrict" runat="server" Enabled="False"></asp:DropDownList><br />
    <asp:Label ID="LabelCreateStaff" runat="server" Text="CreateStaff"></asp:Label>
    <asp:TextBox ID="txtCreateStaff" runat="server" Enabled="False"></asp:TextBox><br />
    <asp:Label ID="LabelIntervention" runat="server" Text="Intervetion"></asp:Label>
        <asp:Label ID="LabelAlert" runat="server" ForeColor="Red" Visible="False"></asp:Label>
    <asp:DropDownList ID="ddlIntervention" runat="server" Visible="False" ></asp:DropDownList><br />
    <asp:Button ID="ButtonNewIntervention" runat="server" Text="NewIntervention" OnClick="ButtonNewIntervention_Click" /><asp:Button ID="ButtonInterventionDetail" runat="server" Text="InterventionDetail" OnClick="ButtonInterventionDetail_Click" /><asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
</asp:Panel>
</asp:Content>
