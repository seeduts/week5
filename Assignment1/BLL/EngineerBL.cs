﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment1.DAL;
using System.Data;
using Assignment1.Models;
namespace Assignment1.BLL
{
    public class EngineerBL:UserBL
    {
        // initilaizing properties
        private EngineerDao engineerDao;
        private InterventionTypeDao interventionTypeDao;

        public EngineerBL()
        {
            // object instantiation in constructor
            engineerDao = new EngineerDao();
            interventionTypeDao = new InterventionTypeDao();
        }
    
        //returns list of clients from district Id
       public DataSet viewClients(int DistrictID)
        {
            return engineerDao.getClients(DistrictID);
        }


        //returns list of client detials according to client id
        public ClientModel viewClientDetail(int ClientID)
        {
            ClientModel clientModel = new ClientModel();
            DataTable dt = new DataTable(); // sets dt as datatable object
            dt = engineerDao.getClientsDetail(ClientID);

            //loops through the recrodsets and sets clientModel properties from the recordsets
            foreach (DataRow dr in dt.Rows)
            {
                clientModel.Id = int.Parse(dr[0].ToString()); //covert string to integer
                clientModel.Name = dr[1].ToString();
                clientModel.LocationInfo = dr[2].ToString();
                clientModel.DistrictID =int.Parse(dr[3].ToString());
                clientModel.UserID = int.Parse(dr[4].ToString());
            }
            return clientModel;
        }

        //returns intervertion details from client Id
        public DataTable viewIntervention(int ClientID)
        {
            return engineerDao.getIntervention(ClientID);
        }

        // gets district name form district id
        public DataTable viewDistrictsName(int DistrictID)
        {
          return  districtDao.getDistrictName(DistrictID);
        }


        //checks if client already exists in the database
        public bool createClient(string name, string locationInfo, int districtID, int userID)
        {
            if (engineerDao.checkClientExist(name, locationInfo, districtID) == true) return false;
            else
            return (engineerDao.insertClient(name, locationInfo, districtID, userID));
        }

        //generate list of intervention per user id
        public DataSet viewInterventionList(int UserID)
        {
            return engineerDao.getInterventionList(UserID);
        }

        //update the interventionState
        public bool UpdateInterventionState(string state,int InterventionID)
        {
            
            return engineerDao.updateInterventionState(state, InterventionID);
        }

        // validates the autorization
        public bool checkUpdateAuthority(int InterventionID)
        {
            InterventionModel interventionModel = ViewInterventionDetails(InterventionID);
            InterventionTypeModel interventionTypeModel = getInterventionTypeModel(interventionModel.InterventionTypeID);
            UserModel userModel = base.getUserByStaffName(interventionModel.Proposer);
            if (userModel.MaximumCosts >= interventionTypeModel.EstimatedCosts && userModel.MaximumHours >= interventionTypeModel.EstimatedHours && userModel.MaximumHours >=interventionModel.LabourRequired && userModel.MaximumCosts >= interventionModel.Costs)
                return true;
            else return false;
        }

        //validates the limit for each user type
        public bool checkCreateAuthority(float hours,float costs,string proposer,int InterventionTypeID)
        {
            UserModel userModel = base.getUserByStaffName(proposer);
            InterventionTypeModel interventionTypeModel = getInterventionTypeModel(InterventionTypeID);
            if (userModel.MaximumCosts >= costs && userModel.MaximumHours >= hours && userModel.MaximumHours >= interventionTypeModel.EstimatedHours && userModel.MaximumCosts >= interventionTypeModel.EstimatedCosts) return true;
            else return false;
        }


        public InterventionTypeModel getInterventionTypeModel(int interventionTypeID)
        {
            InterventionTypeModel model = new InterventionTypeModel();
            DataTable dt = new DataTable();
            dt = interventionTypeDao.getInterventionTypeDetail(interventionTypeID);
            foreach (DataRow dr in dt.Rows)
            {
                model.ID =int.Parse(dr[0].ToString());
                model.Name = dr[1].ToString();
                model.EstimatedHours = int.Parse(dr[2].ToString());
                model.EstimatedCosts = int.Parse(dr[3].ToString());
            }
            return model;
        }


        //generate list of intervention
        public DataTable ViewInterventionType()
        {
            return engineerDao.getInterventionType();
        }

        // create intervention
        public bool CreateIntervenion(InterventionModel interventionModel)
        {
            if (interventionModel.InterventionState!="Proposed")
            {
                return engineerDao.insertIntervention(interventionModel.ClientID, interventionModel.InterventionTypeID, interventionModel.LabourRequired, interventionModel.Costs, interventionModel.ProposerID, interventionModel.InterventionState, interventionModel.PerformDate, interventionModel.Notes, interventionModel.ProposerID);
            }
            else
                return engineerDao.insertIntervention(interventionModel.ClientID, interventionModel.InterventionTypeID, interventionModel.LabourRequired, interventionModel.Costs, interventionModel.ProposerID, interventionModel.InterventionState, interventionModel.PerformDate, interventionModel.Notes);
        }


        //shows intervention details as per client id and intervention type id
        public InterventionModel ViewInterventionDetails(int ClientID, int interventionTypeID)
        {
            InterventionModel interventionModel = new InterventionModel();
            DataTable dt = new DataTable();
            dt = engineerDao.getInterventionDetail(ClientID, interventionTypeID);
            foreach(DataRow dr in dt.Rows)
            {
                interventionModel.ID =int.Parse(dr[0].ToString());
                interventionModel.InterventionTypeID = int.Parse(dr[1].ToString());
                interventionModel.ClientID = int.Parse(dr[2].ToString());
                interventionModel.LabourRequired = int.Parse(dr[3].ToString());
                interventionModel.Costs = int.Parse(dr[4].ToString());
                interventionModel.ProposerID = int.Parse(dr[5].ToString());
                interventionModel.PerformDate = DateTime.Parse(dr[6].ToString());
                interventionModel.InterventionState = dr[7].ToString();
                if (dr[8].ToString().Length > 0)
                {
                    interventionModel.ApproverID = int.Parse(dr[8].ToString());
                    interventionModel.Approver = getStaffName(interventionModel.ApproverID);
                }
                if (dr[9].ToString().Length > 0) interventionModel.Notes = dr[9].ToString();
                if (dr[10].ToString().Length > 0) interventionModel.LifeRemaining =int.Parse(dr[10].ToString());
                if (dr[11].ToString().Length > 0) interventionModel.RecentVisitDate = DateTime.Parse(dr[11].ToString());
                interventionModel.Proposer = getStaffName(interventionModel.ProposerID);
            }
            return interventionModel;
        }


        //generates intevention details
        public InterventionModel ViewInterventionDetails(int InterventionID)
        {
            InterventionModel interventionModel = new InterventionModel();
            DataTable dt = new DataTable();
            dt = engineerDao.getInterventionDetail(InterventionID);
            foreach (DataRow dr in dt.Rows)
            {
                interventionModel.ID = int.Parse(dr[0].ToString());
                interventionModel.InterventionTypeID = int.Parse(dr[1].ToString());
                interventionModel.ClientID = int.Parse(dr[2].ToString());
                interventionModel.LabourRequired = int.Parse(dr[3].ToString());
                interventionModel.Costs = int.Parse(dr[4].ToString());
                interventionModel.ProposerID = int.Parse(dr[5].ToString());
                interventionModel.PerformDate = DateTime.Parse(dr[6].ToString());
                interventionModel.InterventionState = dr[7].ToString();
                if (dr[8].ToString().Length > 0)
                {
                    interventionModel.ApproverID = int.Parse(dr[8].ToString());
                    interventionModel.Approver = getStaffName(interventionModel.ApproverID);
                }
                if (dr[9].ToString().Length > 0) interventionModel.Notes = dr[9].ToString();
                if (dr[10].ToString().Length > 0) interventionModel.LifeRemaining = int.Parse(dr[10].ToString());
                if (dr[11].ToString().Length > 0) interventionModel.RecentVisitDate = DateTime.Parse(dr[11].ToString());
                interventionModel.Proposer = getStaffName(interventionModel.ProposerID);
            }
            return interventionModel;
        }

        //update interventions
        public bool QualityManagement(string notes, int lifeRemaining, DateTime recentVisit, int interventionID)
        {
            return engineerDao.UpdateInterventionDetail(notes, lifeRemaining, recentVisit, interventionID);
        }
    }
}