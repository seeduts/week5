﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AccountantDefault.aspx.cs" Inherits="Assignment1.AcccountantDefault" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .Logout {
            color: red;
            text-decoration: underline;
        }

            .Logout:hover {
                cursor: pointer;
            }
    </style>
    <link href="Css/Menu.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <form runat="server">

        <asp:Menu ID="menu1" CssClass="menu"   Orientation="Horizontal" runat="server">
            <Items>
                <asp:MenuItem NavigateUrl="~/ChangePassword.aspx" Text="ChangePassword" Value="ChangePassword"></asp:MenuItem>
                <asp:MenuItem Text="List All Staff" Value="List All Staff"></asp:MenuItem>
                <asp:MenuItem Text="Run report" Value="Run report"></asp:MenuItem>
                <asp:MenuItem Text="Log Out" Value="Logout"></asp:MenuItem>
            </Items>

        </asp:Menu>

        <asp:Label ID="labelWelcome" runat="server" Text=""></asp:Label>
        <br />
        <asp:LinkButton ID="LinkButton1" runat="server"
            CssClass="Logout" OnClick="LinkButton1_Click">Logout</asp:LinkButton>
        <br />
        <br />
    </form>
</asp:Content>
