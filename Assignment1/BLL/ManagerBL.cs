﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment1.Models;
using Assignment1.DAL;
using System.Data;

namespace Assignment1.BLL
{
    public class ManagerBL:UserBL
    {
        // public UserModel 
        private ManagerDao managerDao;
        private InterventionTypeDao interventionTypeDao;
        private EngineerDao engineerDao;
        public ManagerBL()
        {
            managerDao = new ManagerDao();
            engineerDao = new EngineerDao();
            interventionTypeDao = new InterventionTypeDao();
        }

        public bool checkAuthority(string approver,int InterventionID)
        {
            InterventionModel interventionModel = ViewInterventionDetails(InterventionID);
            UserModel userModel = base.getUserByUserName(approver);
            InterventionTypeModel interventionTypeModel = getInterventionTypeModel(interventionModel.InterventionTypeID);
            if (userModel.MaximumCosts >= interventionTypeModel.EstimatedCosts && userModel.MaximumHours >= interventionTypeModel.EstimatedHours && userModel.MaximumHours >= interventionModel.LabourRequired && userModel.MaximumCosts >= interventionModel.Costs)
                return true;
            else return false;

        }

        public InterventionModel ViewInterventionDetails(int InterventionID)
        {
            InterventionModel interventionModel = new InterventionModel();
            DataTable dt = new DataTable();
            dt = engineerDao.getInterventionDetail(InterventionID);
            foreach (DataRow dr in dt.Rows)
            {
                interventionModel.ID = int.Parse(dr[0].ToString());
                interventionModel.InterventionTypeID = int.Parse(dr[1].ToString());
                interventionModel.ClientID = int.Parse(dr[2].ToString());
                interventionModel.LabourRequired = int.Parse(dr[3].ToString());
                interventionModel.Costs = int.Parse(dr[4].ToString());
                interventionModel.ProposerID = int.Parse(dr[5].ToString());
                interventionModel.PerformDate = DateTime.Parse(dr[6].ToString());
                interventionModel.InterventionState = dr[7].ToString();
                if (dr[8].ToString().Length > 0)
                {
                    interventionModel.ApproverID = int.Parse(dr[8].ToString());
                    interventionModel.Approver = getStaffName(interventionModel.ApproverID);
                }
                if (dr[9].ToString().Length > 0) interventionModel.Notes = dr[9].ToString();
                if (dr[10].ToString().Length > 0) interventionModel.LifeRemaining = int.Parse(dr[10].ToString());
                if (dr[11].ToString().Length > 0) interventionModel.RecentVisitDate = DateTime.Parse(dr[11].ToString());
                interventionModel.Proposer = getStaffName(interventionModel.ProposerID);
            }
            return interventionModel;
        }
        public InterventionTypeModel getInterventionTypeModel(int interventionTypeID)
        {
            InterventionTypeModel model = new InterventionTypeModel();
            DataTable dt = new DataTable();
            dt = interventionTypeDao.getInterventionTypeDetail(interventionTypeID);
            foreach (DataRow dr in dt.Rows)
            {
                model.ID = int.Parse(dr[0].ToString());
                model.Name = dr[1].ToString();
                model.EstimatedHours = int.Parse(dr[2].ToString());
                model.EstimatedCosts = int.Parse(dr[3].ToString());
            }
            return model;
        }
    }
}