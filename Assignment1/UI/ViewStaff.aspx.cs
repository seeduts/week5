﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;

namespace Assignment1
{
    public partial class ViewStaff : System.Web.UI.Page
    {
        AccountBL accountBL = new AccountBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Menu)Master.FindControl("menuAccount")).Visible = true;
            if (Session["UserName"] == null)
                Response.Redirect("Login.aspx");
                StaffList.DataSource = accountBL.viewStaff();
                StaffList.DataBind();
            if (!IsPostBack)
            {
                ddlDistrict.DataSource = accountBL.getDistricts();
                ddlDistrict.DataTextField = "Name";
                ddlDistrict.DataValueField = "ID";
                ddlDistrict.DataBind();
            }
        }

        protected void StaffList_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeDistrictPanel.Visible = true;
            GridViewRow row =StaffList.SelectedRow;
            string name = row.Cells[1].Text.Trim();
            txtStaffName.Text = name;
            ddlDistrict.SelectedValue = accountBL.getStaffDistrictId(name).ToString();
        }

        protected void buttonSave_Click(object sender, EventArgs e)
        {
            int districtId= Convert.ToInt32(ddlDistrict.SelectedItem.Value);
            if (accountBL.changeStaffDistrict(txtStaffName.Text,districtId))
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "<script language='javascript'>alert('Save Successful!')</script>");
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}