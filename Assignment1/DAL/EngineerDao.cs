﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1.DAL
{

    /// <summary>
    /// This is a derived class inherited from UserDao used to access and update engineer data from database
    /// </summary>
    public class EngineerDao : UserDao
    {
        /// <summary>
        /// Retrieve the data of clients in a district given the ID of  the district
        /// </summary>
        public DataSet getClients(int DistrictID)
        {
            DataSet ds = new DataSet();
            string query = "Select ID,Name,LocationInfo from Clients where DistrictID=@DistrictID";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@DistrictID", SqlDbType.Int);
            sqlParameters[0].Value = DistrictID;
            return conn.executeSelectDataSetQuery(query, sqlParameters);
        }

        /// <summary>
        /// get the data of a client given the ID of the client
        /// </summary>
        public DataTable getClientsDetail(int ClientId)
        {
            string query = "Select * from Clients where ID = @id";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@id", SqlDbType.Int);
            sqlParameters[0].Value = ClientId;
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <summary>
        /// get the data of the intenvention related to a client given the id of the client 
        /// </summary>
        public DataTable getIntervention(int ClientID)
        {
            string query = "Select Intervention.ID,InterventionTypes.ID as TypesID, InterventionTypes.Name as TypesName from Intervention inner join InterventionTypes on Intervention.InterventionTypeID=InterventionTypes.ID where ClientID=@ClientID";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@ClientID", SqlDbType.Int);
            sqlParameters[0].Value = ClientID;
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <summary>
        /// Given the client name, location and district, check whether the client exist in database
        /// </summary>
        /// <param name="name"></param>
        /// <param name="locationInfo"></param>
        /// <param name="districtID"></param>
        public bool checkClientExist(string name, string locationInfo, int districtID)
        {
            string query = "select count(*) from Clients where Name=@name and LocationInfo=@locationInfo and DistrictID=@districtID";
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@name", SqlDbType.NChar);
            sqlParameters[0].Value = name;
            sqlParameters[1] = new SqlParameter("@locationInfo", SqlDbType.NChar);
            sqlParameters[1].Value = locationInfo;
            sqlParameters[2] = new SqlParameter("@districtID", SqlDbType.Int);
            sqlParameters[2].Value = districtID;
            if (conn.checkDuplicate(query, sqlParameters) == 0) return false;
            else return true;
        }

        /// <summary>
        /// Insert a new client into database with the detail
        /// </summary>
        public bool insertClient(string name, string locationInfo, int districtID, int userID)
        {
            string query = "Insert into Clients (Name,LocationInfo,DistrictID,UserID) VALUES (@name,@locationInfo,@districtID,@userID)";
            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@name", SqlDbType.NChar);
            sqlParameters[0].Value = name;
            sqlParameters[1] = new SqlParameter("@locationInfo", SqlDbType.NChar);
            sqlParameters[1].Value = locationInfo;
            sqlParameters[2] = new SqlParameter("@districtID", SqlDbType.Int);
            sqlParameters[2].Value = districtID;
            sqlParameters[3] = new SqlParameter("@userID", SqlDbType.Int);
            sqlParameters[3].Value = userID;
            return conn.executeInsertQuery(query, sqlParameters);
        }

        /// <summary>
        /// List the data of interventions given the user id
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public DataSet getInterventionList(int UserID)
        {
            string query = "Select Intervention.ID, Clients.Name as ClientName, InterventionTypes.Name as IntervetionType,Intervention.InterventionState from Intervention inner join InterventionTypes on Intervention.InterventionTypeID=InterventionTypes.ID inner join Clients on Clients.ID=InterveNtion.ClientID where ProposerID=@UserID";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@UserID", SqlDbType.Int);
            sqlParameters[0].Value = UserID;
            return conn.executeSelectDataSetQuery(query, sqlParameters);
        }
        /// <summary>
        /// update the state of an intervention 
        /// </summary>
        public bool updateInterventionState(string state, int InterventionID)
        {
            string query = "Update Intervention set InterventionState=@state where ID=@InterventionID";
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@state", SqlDbType.NChar);
            sqlParameters[0].Value = state;
            sqlParameters[1] = new SqlParameter("@InterventionID", SqlDbType.Int);
            sqlParameters[1].Value = InterventionID;
            return (conn.executeUpdateQuery(query, sqlParameters));
        }

        /// <summary>
        /// Get  all the intervention types
        /// </summary>
        /// <returns></returns>
        public DataTable getInterventionType()
        {
            string query = "Select * from InterventionTypes";
            return conn.executeSelectQuery(query);
        }
        /// <summary>
        /// insert a new intervention with approve ID
        /// </summary>
        public bool insertIntervention(int ClientID, int InterventionTypeID, float Labour, float cost, int userID, string state, DateTime performDate, string notes, int ApproverID)
        {
            string query = "Insert into Intervention (InterventionTypeID,ClientID,LabourRequired,Costs,InterventionState,ProposerID,PerformDate,Notes,ApproverID) VALUES (@InterventionTypeID,@ClientID,@Labour,@cost,@State,@userID,@performDate,@notes,@approverid)";
            SqlParameter[] sqlParameters = new SqlParameter[9];
            sqlParameters[0] = new SqlParameter("@ClientID", SqlDbType.Int);
            sqlParameters[0].Value = ClientID;
            sqlParameters[1] = new SqlParameter("@InterventionTypeID", SqlDbType.Int);
            sqlParameters[1].Value = InterventionTypeID;
            sqlParameters[2] = new SqlParameter("@Labour", SqlDbType.Int);
            sqlParameters[2].Value = Labour;
            sqlParameters[3] = new SqlParameter("@cost", SqlDbType.Int);
            sqlParameters[3].Value = cost;
            sqlParameters[4] = new SqlParameter("@userID", SqlDbType.Int);
            sqlParameters[4].Value = userID;
            sqlParameters[5] = new SqlParameter("@State", SqlDbType.NChar);
            sqlParameters[5].Value = state;
            sqlParameters[6] = new SqlParameter("@performDate", SqlDbType.Date);
            sqlParameters[6].Value = performDate;
            sqlParameters[7] = new SqlParameter("@notes", SqlDbType.Text);
            sqlParameters[7].Value = notes;
            sqlParameters[8] = new SqlParameter("@approverid", SqlDbType.Int);
            sqlParameters[8].Value = ApproverID;
            return conn.executeInsertQuery(query, sqlParameters);
        }
        /// <summary>
        /// insert an intervention  without apporve id
        /// </summary>
        public bool insertIntervention(int ClientID, int InterventionTypeID, float Labour, float cost, int userID, string state, DateTime performDate, string notes)
        {
            string query = "Insert into Intervention (InterventionTypeID,ClientID,LabourRequired,Costs,InterventionState,ProposerID,PerformDate,Notes) VALUES (@InterventionTypeID,@ClientID,@Labour,@cost,@State,@userID,@performDate,@notes)";
            SqlParameter[] sqlParameters = new SqlParameter[8];
            sqlParameters[0] = new SqlParameter("@ClientID", SqlDbType.Int);
            sqlParameters[0].Value = ClientID;
            sqlParameters[1] = new SqlParameter("@InterventionTypeID", SqlDbType.Int);
            sqlParameters[1].Value = InterventionTypeID;
            sqlParameters[2] = new SqlParameter("@Labour", SqlDbType.Int);
            sqlParameters[2].Value = Labour;
            sqlParameters[3] = new SqlParameter("@cost", SqlDbType.Int);
            sqlParameters[3].Value = cost;
            sqlParameters[4] = new SqlParameter("@userID", SqlDbType.Int);
            sqlParameters[4].Value = userID;
            sqlParameters[5] = new SqlParameter("@State", SqlDbType.NChar);
            sqlParameters[5].Value = state;
            sqlParameters[6] = new SqlParameter("@performDate", SqlDbType.Date);
            sqlParameters[6].Value = performDate;
            sqlParameters[7] = new SqlParameter("@notes", SqlDbType.Text);
            sqlParameters[7].Value = notes;
            return conn.executeInsertQuery(query, sqlParameters);
        }

        /// <summary>
        /// get the detail of intervention given the client id and intervention type
        /// </summary>
        public DataTable getInterventionDetail(int ClientID, int interventionTypeID)
        {
            string query = "select * from Intervention where ClientID=@ClientID and InterventionTypeID=@interventionTypeID";
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@ClientID", SqlDbType.Int);
            sqlParameters[0].Value = ClientID;
            sqlParameters[1] = new SqlParameter("@interventionTypeID", SqlDbType.Int);
            sqlParameters[1].Value = interventionTypeID;
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <summary>
        /// get the intervention detail given the intervention ID
        /// </summary>
        public DataTable getInterventionDetail(int InterventionID)
        {
            string query = "select * from Intervention where ID=@InterventionID";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@InterventionID", SqlDbType.Int);
            sqlParameters[0].Value = InterventionID;
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <summary>
        /// update the detail of an intervention
        /// </summary>
        public bool UpdateInterventionDetail(string notes, int lifeRemaining, DateTime recentVisit, int interventionID)
        {
            string query = "Update Intervention set Notes=@notes,LifeRemaining=@lifeRemaining,RecentVisitDate=@recentVisit where ID=@interventionID";
            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@notes", SqlDbType.Text);
            sqlParameters[0].Value = notes;
            sqlParameters[1] = new SqlParameter("@lifeRemaining", SqlDbType.Int);
            sqlParameters[1].Value = lifeRemaining;
            sqlParameters[2] = new SqlParameter("@recentVisit", SqlDbType.Date);
            sqlParameters[2].Value = recentVisit;
            sqlParameters[3] = new SqlParameter("@interventionID", SqlDbType.Int);
            sqlParameters[3].Value = interventionID;
            return conn.executeUpdateQuery(query, sqlParameters);
        }


    }
}