﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Assignment1.DAL
{
    /// <summary>
    /// A relatively reusable database access class. It is AutoCloseable and
    /// designed to be used with the try-with-resource construct.
    /// </summary>
    public class dbConnection
    {
        private SqlConnection conn;
        private SqlDataAdapter myAdapter;


        public dbConnection()
        {
            myAdapter = new SqlDataAdapter();
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
        }

        private SqlConnection openConnection()
        {
            if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
            {
                conn.Open();
            }
            return conn;
        }

        public DataTable executeSelectQuery(string query, SqlParameter[] sqlParamater)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParamater);
                cmd.ExecuteNonQuery();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(dt);
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeSelectQuery - Query:" + query + " \nException: " + e.StackTrace.ToString());
            }
            return dt;
        }

        public DataTable executeSelectQuery(string query)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(dt);
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeSelectQuery - Query:" + query + " \nException: " + e.StackTrace.ToString());
            }
            return dt;
        }

        public DataSet executeSelectDataSetQuery(string query)
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(ds);
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeSelectQuery - Query:" + query + " \nException: " + e.StackTrace.ToString());
            }
            return ds;
        }

        public DataSet executeSelectDataSetQuery(string query, SqlParameter[] sqlParameter)
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParameter);
                cmd.ExecuteNonQuery();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(ds);
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeSelectQuery - Query:" + query + " \nException: " + e.StackTrace.ToString());
            }
            return ds;
        }
        public bool executeInsertQuery(string query, SqlParameter[] sqlParameter)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = cmd;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeInsertQuery - Query:" + query + " \nException: " + e.StackTrace.ToString());
            }
            return true;
        }
        public bool executeUpdateQuery(string query, SqlParameter[] sqlParameter)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParameter);
                myAdapter.UpdateCommand = cmd;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeUpdateQuery - Query:" + query + " \nException: " + e.StackTrace.ToString());
            }
            return true;
        }

        public int checkDuplicate(string query, SqlParameter[] sqlParameter)
        {
            SqlCommand cmd = new SqlCommand();
            int i = 0;
            try
            {
                cmd.Connection = openConnection();
                cmd.CommandText = query;
                cmd.Parameters.AddRange(sqlParameter);
                myAdapter.SelectCommand = cmd;
                i = (int)cmd.ExecuteScalar();

            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeUpdateQuery - Query:" + query + " \nException: " + e.StackTrace.ToString());
            }
            return i;
        }
    }
}