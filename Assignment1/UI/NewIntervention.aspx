﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.Master" AutoEventWireup="true" CodeBehind="NewIntervention.aspx.cs" Inherits="Assignment1.CreateIntervention" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #TextArea1 {
            width: 234px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <asp:Label ID="LabelClient" runat="server" Text="Client"></asp:Label>
    <asp:TextBox ID="txtClient" runat="server" Enabled="False"></asp:TextBox><br />
    <asp:Label ID="LabelInterventionType" runat="server" Text="InterventionType"></asp:Label>
    <asp:DropDownList ID="ddlInterventionType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlInterventionType_SelectedIndexChanged"></asp:DropDownList>
    <br />
    <asp:Label ID="LabelLabourRequired" runat="server" Text="Labour Required (Hours)"></asp:Label>
    <asp:TextBox ID="txtLabourRequired" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="The required hours must be greater than 0."  ForeColor="Red" ControlToValidate="txtLabourRequired" ValidationExpression="^[1-9][0-9]*$"></asp:RegularExpressionValidator>
    <br />
     <asp:Label ID="LabelCostRequired" runat="server" Text="Cost Required (AUD$)"></asp:Label>
    <asp:TextBox ID="txtCostRequired" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="The required hours must be greater than 0." ForeColor="Red"  ControlToValidate="txtCostRequired" ValidationExpression="^[1-9][0-9]*$"></asp:RegularExpressionValidator>
    <br />
     <asp:Label ID="LabelProposer" runat="server" Text="Proposer"></asp:Label>
    <asp:TextBox ID="txtProposer" runat="server" Enabled="false"></asp:TextBox><br />
    <asp:Label ID="LabelPerformDate" runat="server" Text="Perform Date"></asp:Label>
    <asp:Calendar ID="CalendarPerformDate" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" ShowGridLines="True" Width="220px">
        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
        <OtherMonthDayStyle ForeColor="#CC9966" />
        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
        <SelectorStyle BackColor="#FFCC66" />
        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
    </asp:Calendar>
    <asp:CustomValidator ID="PerformDateValidator" runat="server" ForeColor="Red"  OnServerValidate="PerformDateValidator_ServerValidate"></asp:CustomValidator>
    <br />
    <asp:Label ID="LabelState" runat="server" Text="State"></asp:Label>
    <asp:DropDownList ID="ddlState" runat="server" Enabled="False">
        <asp:ListItem>Proposed</asp:ListItem>
        <asp:ListItem>Approved</asp:ListItem>
        <asp:ListItem>Completed</asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="ButtonCheck" runat="server"  Text="CheckAuthority" OnClick="ButtonCheck_Click" />
    <asp:Label ID="LabelCheck" runat="server" Forecolor="Red"></asp:Label>
    <br />
    <asp:Label ID="LabelComments" runat="server" Text="Comments"></asp:Label><br />
    <asp:TextBox id="txtComments" TextMode="multiline" Columns="50" Rows="5" runat="server" /><br />
    <br />
    <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
</asp:Content>
