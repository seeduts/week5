﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Assignment1.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
    Login
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Login.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <section class="Login">
<div class="form_title">Staff Login</div>
     <div class="form">
    <asp:TextBox ID="TxtUsername" placeholder="Username"  runat="server" CssClass="Login_name" MaxLength="20"></asp:TextBox>
    <asp:TextBox ID="TxtPassword" placeholder="Password"   runat="server" CssClass="Login_password" TextMode="Password" MaxLength="20" ></asp:TextBox>
    <asp:Button ID="buttonReset" Text="Reset" runat="server" CssClass="Reset" OnClick="buttonReset_Click"/>
    <asp:Button ID="buttonLogin" Text="Login" runat="server" CssClass="Submit" OnClick="buttonLogin_Click"/>
         </div>
</section>
</asp:Content>
