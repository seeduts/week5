﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Assignment1.DAL;

namespace Assignment1.BLL
{
    public class AccountBL:UserBL
    {
        private AccountDao accountDao;
        public AccountBL()
        {
            //objects inisilization
            accountDao = new AccountDao();
            districtDao = new DistrictDao();
        }
        public DataSet viewStaff()
        {
            //returns list of staff from the database
           return accountDao.getStaff();
        }


        public int getStaffDistrictId(string name)
        {
            // returns staff district id from staff name
            return districtDao.getStaffDistrictID(name);
        }

        public bool changeStaffDistrict(string name,int districtId)
        {
            //retruns true or false on sucess and faliure on updating staff's district
            return accountDao.UpdateStaffDistrict(name, districtId);
        }

        public DataSet ViewTotalCostsByEnigneer()
        {
            //return total cost
            return accountDao.getTotalCosts();
        }

        public DataSet ViewAverageCostsByEngineer()
        {
            return accountDao.getAverageCosts();
        }
        public DataSet ViewDistrictCosts()
        {
            return accountDao.getDistrictsCosts();
        }
        public DataSet ViewMonthlyCosts()
        {
            return accountDao.getMonthlyCosts();
        }
    }
}