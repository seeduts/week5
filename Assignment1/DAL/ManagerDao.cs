﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1.DAL
{
    public class ManagerDao:UserDao
    {
        /*public DataSet ListIntervention(int userID)
        {
            string query = "select * from Interventions";
            return ;
        }*/

        public bool UpdateIntervention(int userID,int interventionID)
        {
            string query = "Update Interventions set InterventionState=Approved,ApproverID=@userID where ID=@interventionID";
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@userID", SqlDbType.Int);
            sqlParameters[0].Value = userID;
            sqlParameters[1] = new SqlParameter("@InterventionTypeID", SqlDbType.Int);
            sqlParameters[1].Value = interventionID;
            return conn.executeUpdateQuery(query, sqlParameters);
        }
    }
}