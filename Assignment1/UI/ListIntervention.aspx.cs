﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment1.BLL;
using Assignment1.Models;

namespace Assignment1
{
    public partial class ListIntervention : System.Web.UI.Page
    {
        EngineerBL engineerBL = new EngineerBL();
        InterventionModel interventionmodel;
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Menu)Master.FindControl("menuEngineer")).Visible = true;
            if (Session["UserName"] == null)
                Response.Redirect("Login.aspx");
            int userID = Convert.ToInt32(Session["userID"]);
            if (!IsPostBack)
            {
                if (engineerBL.viewInterventionList(userID).Tables[0].Rows.Count > 0)
                {
                    ViewIntervention.DataSource = engineerBL.viewInterventionList(userID);
                    ViewIntervention.DataBind();
                }
                else
                {
                    Message.Text = "You have not create any Interventions.";
                }
            }
        }

        protected void ViewIntervention_SelectedIndexChanged(object sender, EventArgs e)
        {
            panelModify.Visible = true;
            GridViewRow row = ViewIntervention.SelectedRow;
            int InterventionID = int.Parse(row.Cells[1].Text);
            interventionmodel = engineerBL.ViewInterventionDetails(InterventionID);
            txtLabourRequired.Text = interventionmodel.LabourRequired.ToString();
            txtCostRequired.Text = interventionmodel.Costs.ToString();
            txtProposer.Text = interventionmodel.Proposer;
            CalendarPerformDate.SelectedDate = interventionmodel.PerformDate;
            CalendarPerformDate.VisibleDate = CalendarPerformDate.SelectedDate;
            txtApprover.Text = interventionmodel.Approver;
            txtComments.Text = interventionmodel.Notes;
            txtLifeRemaining.Text = interventionmodel.LifeRemaining.ToString();
            Calendar2.VisibleDate = interventionmodel.RecentVisitDate;
            Calendar2.SelectedDate = interventionmodel.RecentVisitDate;
            ddlState.SelectedValue = row.Cells[4].Text.Trim();
            txtClient.Text = row.Cells[2].Text.Trim();
            txtInterventionType.Text = row.Cells[3].Text;
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            GridViewRow row = ViewIntervention.SelectedRow;
            int InterventionID =Convert.ToInt32(row.Cells[1].Text.Trim());
            string state = ddlState.SelectedValue;
            if (engineerBL.UpdateInterventionState(state, InterventionID) ==false)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "<script language='javascript'>alert('Change Intervention State failed!')</script>");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "redirect",
                     "alert('Change Intervention Stat successfully'); window.location='" +
                     Request.ApplicationPath + "UI/Default.aspx';", true);
            }
        }
    }
}