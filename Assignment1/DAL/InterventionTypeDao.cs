﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Assignment1.DAL
{
    /// <summary>
    /// This is used to access and update intervention type data from database
    /// </summary>
    public class InterventionTypeDao
    {
        private dbConnection conn;
        public InterventionTypeDao()
        {
            conn = new dbConnection();
        }
        /// <summary>
        /// Get the data of the intervention type given the ID of the type
        /// </summary>
        /// <param name="interventionTypeId"></param>
        /// <returns></returns>
        public DataTable getInterventionTypeDetail(int interventionTypeId)
        {
            string query = "Select * from InterventionTypes where ID=@id";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@id", SqlDbType.Int);
            sqlParameters[0].Value = interventionTypeId;
            return conn.executeSelectQuery(query, sqlParameters);
        }
    }
}