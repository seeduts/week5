﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/MasterPage.Master" AutoEventWireup="true" CodeBehind="ViewStaff.aspx.cs" Inherits="Assignment1.ViewStaff" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
    <asp:GridView ID="StaffList" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" OnSelectedIndexChanged="StaffList_SelectedIndexChanged">
        <Columns>
            <asp:CommandField HeaderText="View Details" ShowHeader="True" ShowSelectButton="True" SelectText="Edit" />
        </Columns>
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FFF1D4" />
        <SortedAscendingHeaderStyle BackColor="#B95C30" />
        <SortedDescendingCellStyle BackColor="#F1E5CE" />
        <SortedDescendingHeaderStyle BackColor="#93451F" />
</asp:GridView>
      <asp:Panel ID="changeDistrictPanel" runat="server" Visible="false">
        <asp:Label ID="LabelStaffName" runat="server" Text="StaffName"></asp:Label>
        <asp:TextBox ID="txtStaffName" runat="server" Enabled="false"></asp:TextBox><br />
        <asp:Label ID="LabelDistrict" runat="server" Text="District"></asp:Label>
        <asp:DropDownList ID="ddlDistrict" runat="server"></asp:DropDownList><br />
        <asp:Button ID="buttonSave" runat="server" Text="Save" OnClick="buttonSave_Click" />
          <asp:Button ID="ButtonCancel" runat="server" OnClick="ButtonCancel_Click" Text="Cancel" />
    </asp:Panel>
</asp:Content>
