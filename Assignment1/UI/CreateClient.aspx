﻿<%@ Page Title="" Language="C#" MasterPageFile="../UI/MasterPage.Master" AutoEventWireup="true" CodeBehind="CreateClient.aspx.cs" Inherits="Assignment1.CreateClient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="css/Default.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="server">
        <asp:Label ID="LabelClientName" runat="server" Text="ClientName"></asp:Label>
    <asp:TextBox ID="txtClientname" runat="server"></asp:TextBox><br />
        <asp:Label ID="LabelLocationInfo" runat="server" Text="LocationInfo"></asp:Label>
    <asp:TextBox ID="txtLocation" runat="server"></asp:TextBox><br />
        <asp:Label ID="LabelDistrict" runat="server" Text="District"></asp:Label>
    <asp:dropDownList ID="ddlDistrict" runat="server" Enabled="False" Width="150px" ></asp:dropDownList><br />
        <br />
    <asp:Button ID="buttonCancel" Text="Cancel" runat="server" OnClick="buttonCancel_Click" style="height: 26px" />
    <asp:Button ID="buttonSubmit" Text="Submit" runat="server" OnClick="buttonSubmit_Click" style="height: 26px" />
</asp:Content>
