﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1.Models
{
    /// <summary>
    ///  A model object for client, storing their id, name,
    /// locationInfo ,districtID and userID.
    /// </summary>
    public class ClientModel
    {
        private int id;
        private string name;
        private string locationInfo;
        private int districtID;
        private int userID;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string LocationInfo
        {
            get
            {
                return locationInfo;
            }

            set
            {
                locationInfo = value;
            }
        }

        public int DistrictID
        {
            get
            {
                return districtID;
            }

            set
            {
                districtID = value;
            }
        }

        public int UserID
        {
            get
            {
                return userID;
            }

            set
            {
                userID = value;
            }
        }
    }
}