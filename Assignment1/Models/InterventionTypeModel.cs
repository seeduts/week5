﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1.Models
{
    /// <summary>
    ///  A model object for intervention type, storing their id, name,
    /// estimatedHours and estimatedCosts.
    /// </summary>
    public class InterventionTypeModel
    {
        private int iD;
        private string name;
        private float estimatedHours;
        private float estimatedCosts;

        public float EstimatedCosts
        {
            get
            {
                return estimatedCosts;
            }

            set
            {
                estimatedCosts = value;
            }
        }

        public float EstimatedHours
        {
            get
            {
                return estimatedHours;
            }

            set
            {
                estimatedHours = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }
    }
}