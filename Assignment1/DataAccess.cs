﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace Assignment1.DAL
{
    public class DataAccess
    {
        string conn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public DataTable Login(string userName, string password)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                MD5 md5Hash = MD5.Create();
                string md5 = "0x" + GetMd5Hash(md5Hash, password);
                string queryString = "Select * from Users where UserName=@userName COLLATE SQL_Latin1_General_CP1_CS_AS and Password=@password COLLATE SQL_Latin1_General_CP1_CS_AS";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@userName", userName);
                cmd.Parameters.AddWithValue("@password", md5);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                con.Close();
                return dt;
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2").ToUpper());
            }
            return sBuilder.ToString();
        }

        public DataTable getDistricts(int districtId)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select * from Districts where ID=@id";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@id", districtId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                con.Close();
                return dt;
            }
        }

        public DataTable getDistricts()
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select * from Districts";
                SqlCommand cmd = new SqlCommand(queryString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                con.Close();
                return dt;
            }
        }

        public int getDistrictsID(string name)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select * from Users where Name=@name";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                con.Close();
                int districtID = Convert.ToInt32(dt.Rows[0]["DistrictID"]);
                return districtID;
            }
        }

        public int UpdateStaffDistrict(string name, int districtId)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string updateString = "Update Users set DistrictID=@districtId where Name=@name";
                SqlCommand cmd = new SqlCommand(updateString, con);
                cmd.Parameters.AddWithValue("@districtId", districtId);
                cmd.Parameters.AddWithValue("@name", name);
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
        }
        public int insertClient(string name, string locationInfo, int districtID, int userID)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string insertString = "Insert into Clients (Name,LocationInfo,DistrictID,UserID) VALUES (@name,@locationInfo,@districtID,@userID)";
                SqlCommand cmd = new SqlCommand(insertString, con);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@locationInfo", locationInfo);
                cmd.Parameters.AddWithValue("@districtID", districtID);
                cmd.Parameters.AddWithValue("@userID", userID);
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
        }

        public int changePassword(int userID, string password)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                MD5 md5Hash = MD5.Create();
                string md5 = "0x" + GetMd5Hash(md5Hash, password);
                string updateString = "update Users set password=@password where ID=@userID";
                SqlCommand cmd = new SqlCommand(updateString, con);
                cmd.Parameters.AddWithValue("@userID", userID);
                cmd.Parameters.AddWithValue("@password", md5);
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
        }

        public DataSet viewClients(int DistrictID)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select Name,LocationInfo from Clients where DistrictID=@DistrictID";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@DistrictID", DistrictID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
                return ds;
            }
        }

        public DataTable viewClients(string name)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select * from Clients where Name=@name";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                con.Close();
                return dt;
            }
        }

        public DataSet viewStaff()
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select Users.Name as staffName,UserType.Name as Role from Users inner join UserType on Users.UserTypeID=UserType.ID where UserTypeID<>3";
                SqlCommand cmd = new SqlCommand(queryString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
                return ds;
            }
        }

        public string getStaffName(int userID)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select Name from Users where ID=@userID";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@userID", userID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                con.Close();
                string name = dt.Rows[0][0].ToString().Trim();
                return name;
            }
        }

        public DataTable getIntervention(int ClientID)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select Intervention.ID, InterventionTypes.Name from Intervention inner join InterventionTypes on Intervention.InterventionTypeID=InterventionTypes.ID where ClientID=@ClientID";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@ClientID", ClientID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }

        public DataTable getInterventionType()
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select * from InterventionTypes";
                SqlCommand cmd = new SqlCommand(queryString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                con.Close();
                return dt;
            }
        }

        public int CreateNewIntervention(int ClientID,int InterventionTypeID,int Labour,int cost,int userID,string state,DateTime performDate,string notes)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string insertString = "Insert into Intervention (InterventionTypeID,ClientID,LabourRequired,Costs,InterventionState,ProposerID,PerformDate,Notes) VALUES (@InterventionTypeID,@ClientID,@Labour,@cost,@State,@userID,@performDate,@notes) ";
                SqlCommand cmd = new SqlCommand(insertString, con);
                cmd.Parameters.AddWithValue("@ClientID", ClientID);
                cmd.Parameters.AddWithValue("@InterventionTypeID",InterventionTypeID);
                cmd.Parameters.AddWithValue("@Labour", Labour);
                cmd.Parameters.AddWithValue("@cost", cost);
                cmd.Parameters.AddWithValue("@userID", userID);
                cmd.Parameters.AddWithValue("@State", state);
                cmd.Parameters.AddWithValue("@performDate", performDate);
                cmd.Parameters.AddWithValue("@notes", notes);
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
        }

        public DataSet ViewIntervention(int UserID)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string queryString = "Select Intervention.ID, Clients.Name as ClientName, InterventionTypes.Name as IntervetionType,Intervention.InterventionState from Intervention inner join InterventionTypes on Intervention.InterventionTypeID=InterventionTypes.ID inner join Clients on Clients.ID=InterveNtion.ClientID where ProposerID=@UserID";
                SqlCommand cmd = new SqlCommand(queryString, con);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
                return ds;
            }
        }

        public int UpdateInterventionState(string state,int InterventionID)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                con.Open();
                string updateString = "Update Intervention set InterventionState=@state where ID=@InterventionID";
                SqlCommand cmd = new SqlCommand(updateString, con);
                cmd.Parameters.AddWithValue("@state", state);
                cmd.Parameters.AddWithValue("@InterventionID",InterventionID);
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return i;
            }
        }

    }
}